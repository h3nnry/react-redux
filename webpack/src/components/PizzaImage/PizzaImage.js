import React from 'react';
import classes from './PizaImage.css';
import PizzaImage from '../../assets/pizza.jpg';

const pizzaImage = (props) => (
    <div className={classes.PizzaImage}>
        <img src={PizzaImage} alt="" className={classes.PizzaImg} />
    </div>
);

export default pizzaImage;