import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions';

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';

class Counter extends Component {
    state = {
        counter: 0
    }

    counterChangedHandler = ( action, value ) => {
        switch ( action ) {
            case 'inc':
                this.setState( ( prevState ) => { return { counter: prevState.counter + 1 } } )
                break;
            case 'dec':
                this.setState( ( prevState ) => { return { counter: prevState.counter - 1 } } )
                break;
            case 'add':
                this.setState( ( prevState ) => { return { counter: prevState.counter + value } } )
                break;
            case 'sub':
                this.setState( ( prevState ) => { return { counter: prevState.counter - value } } )
                break;
        }
    }

    render () {
        return (
            <div>
                <CounterOutput value={this.props.ctr} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}  />
                <CounterControl label="Add 5" clicked={this.props.onIncrementValueCounter}  />
                <CounterControl label="Subtract 5" clicked={this.props.onDecrementValueCounter}  />
                <hr/>
                <button onClick={() => {this.props.onStoreResult(this.props.ctr)}}>Store Result</button>
                <ul>
                    {this.props.storedResults.map(strResult => {
                        return <li key={strResult.key} onClick={() => this.props.onDeleteResult(strResult.key)}>{strResult.value}</li>
                    })}
                </ul>
            </div>
        );
    }
}

const mapStateProps = state => {
    return {
        ctr: state.ctr.counter,
        storedResults: state.res.results
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({type: actionTypes.INCREMENT}),
        onDecrementCounter: () => dispatch({type: actionTypes.DECREMENT}),
        onIncrementValueCounter: () => dispatch({type: actionTypes.INCREMENT_VALUE, value: 5}),
        onDecrementValueCounter: () => dispatch({type: actionTypes.DECREMENT_VALUE, value: 5}),
        onStoreResult: (counter) => dispatch({type: actionTypes.STORE_RESULT, counter: counter}),
        onDeleteResult: (key) => dispatch({type: actionTypes.DELETE_RESULT, key: key}),
    }
};

export default connect(mapStateProps, mapDispatchToProps)(Counter);