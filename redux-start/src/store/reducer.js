import * as actionTypes from './actions';

const initialState = {
    counter: 0,
    results: []
}

const reducer = (state =  initialState, action) => {
    // const newState = Object.assign({}, state);
    switch (action.type) {
        case actionTypes.INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            }
        case actionTypes.DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            }
        case actionTypes.INCREMENT_VALUE:
            return {
                ...state,
                counter: state.counter + action.value
            }
        case actionTypes.DECREMENT_VALUE:
            return {
                ...state,
                counter: state.counter - action.value
            }
        case actionTypes.STORE_RESULT:
            console.log(state);
            return {
                ...state,
                results: state.results.concat({key: new Date, value: state.counter})
            }
        case actionTypes.DELETE_RESULT:
            // const newState = state.results.splice(1);
            return {
                ...state,
                results: state.results.filter(result => result.key != action.key)
            }
        default:
            return state;
    }
}

export default reducer;