import * as actionTypes from '../actions';

const initialState = {
    counter: 0
}

const counter = (state =  initialState, action) => {
    // const newState = Object.assign({}, state);
    switch (action.type) {
        case actionTypes.INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            }
        case actionTypes.DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            }
        case actionTypes.INCREMENT_VALUE:
            return {
                ...state,
                counter: state.counter + action.value
            }
        case actionTypes.DECREMENT_VALUE:
            return {
                ...state,
                counter: state.counter - action.value
            }
        default:
            return state;
    }
}

export default counter;