import * as actionTypes from '../actions';

const initialState = {
    results: []
}

const results = (state =  initialState, action) => {
    // const newState = Object.assign({}, state);
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            console.log(state);
            return {
                ...state,
                results: state.results.concat({key: new Date, value: action.counter})
            }
        case actionTypes.DELETE_RESULT:
            // const newState = state.results.splice(1);
            return {
                ...state,
                results: state.results.filter(result => result.key != action.key)
            }
        default:
            return state;
    }
}

export default results;